require "LECEngine/LECEngine"

-- A global variable to use the LECEngine's OOP implementation
OO = LECEngine.Core.OOImplementation

local Components = require "Components/Components"

local Entity = LECEngine.Entity
local Vector = LECEngine.Utils.Vector

-- we make a namespace
-- no need to make a whole class, those are just utilities functions
Tests = {}

Tests.init = function ()
  local texture = love.graphics.newImage("assets/man.png")

  local e = Entity.new()
  e.transform.position = Vector.new(love.window.getWidth() / 2, love.window.getHeight() / 2)
  e:addComponent(LECEngine.Components.SpriteRenderer.new(texture))
  --e:addComponent(Components.RandomMove.new(250, 1))
  e:addComponent(Components.FaceMouse.new())
end
