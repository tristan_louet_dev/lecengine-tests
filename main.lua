require "LECEngine/LECEngine"
require "LECEngineTests"

function love.load()
	Tests.init()
end

function love.draw()
	LECEngine.get():draw()
end

function love.update(p_dt)
	LECEngine.get():update(p_dt)
end

function love.keypressed(p_key, p_isRepeat)
	if p_key == 'escape' then
		love.event.quit()
	end
end

function love.keyreleased(p_key, p_isRepeat)
end

function love.textinput(p_text)
end

function love.mousepressed(p_x, p_y, p_button)
end

function love.mousereleased(p_x, p_y, p_button)
end

function love.resize(p_width, p_height)
end

function love.focus(p_isFocused)
end

function love.mousefocus(p_isFocused)
end

function love.visible(p_isFocused)
end

function love.quit()
end
