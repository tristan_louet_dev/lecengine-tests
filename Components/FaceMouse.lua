local FaceMouse = OO.newClassInheritsFrom(LECEngine.LogicComponent)
local Vector = LECEngine.Utils.Vector

function FaceMouse.new ()
  local fm = FaceMouse:create()
  fm:init()
  return fm
end

function init ()
  FaceMouse:superClass().init(self)
end

function FaceMouse:update (p_dt)
  self.transform:faceTo(Vector.new(love.mouse.getX(), love.mouse.getY()))
  self.transform.positon = self.transform:translate(self.transform.forward:mul(100 * p_dt))
end

return FaceMouse
