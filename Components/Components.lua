local Components = {}

Components.FaceMouse = require "Components/FaceMouse"
Components.RandomMove = require "Components/RandomMove"

return Components
