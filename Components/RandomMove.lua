local RandomMove = OO.newClassInheritsFrom(LECEngine.LogicComponent)

function RandomMove.new (p_speed, p_maxAngle)
  local rm = RandomMove:create()
  rm:init()
  rm.speed = p_speed
  rm.maxAngle = p_maxAngle
  return rm
end

function RandomMove:init ()
  RandomMove:superClass().init(self)
  if not self.speed then
    self.speed = 1
  end
  if not self.maxAngle then
    self.maxAngle = 180
  end
end

function RandomMove:update (p_dt)
  self.Transform:rotate(math.random(-self.maxAngle, self.maxAngle))
  self.Transform:translate(self.Transform.forward:mul(self.speed * p_dt))
end

return RandomMove
